﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFTParty_Midterms
{
    class PlayerRoster
    {
        public int Gil { get; set; }
        public List<Character> Characters = new List<Character>();

        public void PrintRoster()
        {
            foreach (Character character in Characters)
            {
                character.PrintMemberDetails();
            }
        }

        /// QUIZ ITEM
        /// Implement the EncounterMonster function, 
        /// this is when a Monster is about attack the party and potentially kill someone.
        /// The Monster's target can be read from monster.CharacterToKill
        /// Search your available party members for a match, if there's a match
        /// Remove that character from your list,
        /// If there isn't a match, just print "Monster wasn't able to kill anything!"
        /// Hint: Linear Search
        public void EncounterMonster(Monster monster)
        {

        }

        /// QUIZ ITEM
        /// Compute the player's money earned
        /// Loop through each character and get their income
        /// Display the income and add it to the player's Gil
        public void ComputeEarnings()
        {
            int earnings = 0;
            foreach (Character a in Characters)
            {
                earnings += a.Income;
            }
            Console.WriteLine("You have earned: " + earnings);
            Gil += earnings;
        }
    }
}
