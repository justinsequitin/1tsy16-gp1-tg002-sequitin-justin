﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ResistanceBase
{
    class Program
    {
        static Random random = new Random();
        static int GetNumSpies(int totalPlayers)
        {
            //for (int x = 1; x <= 5; x += 2)
            //{
            //    if (totalPlayers == x + 4 || totalPlayers == x + 5)
            //    {
            //        Console.Write("\nThe number of Spies are 2");
            //    }

            //    if (totalPlayers == x + 6 || totalPlayers == x + 7)
            //    {
            //        Console.Write("\nThe number of Spies are 3");
            //    }
            //}

            if (totalPlayers >= 5 && totalPlayers <= 6)
                Console.Write("\nThe number of Spies are 2\n");
            else if (totalPlayers >= 7 && totalPlayers <= 8)
                Console.Write("\nThe number of Spies are 3\n");
            if (totalPlayers >= 9 && totalPlayers <= 10)
                Console.Write("\nThe number of Spies are 4\n");
            return 0;
           
            // 5-6 players = 2 spies
            // 7-8 players = 3 spies
            // 9-10 players = 4 spies
            //throw new NotImplementedException();
        }

        static int EvaluateRound(string[] players)
        {
            // Return 0 if resistance wins
            // Return 1 if spies win
            throw new NotImplementedException();
        }

        static void PrintPlayers(string[] players)
        {
            string[] printplayers = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };

            for (int i = 0; i < players.Length; i++)
            {
                Console.WriteLine("Player [" + players[i] + printplayers[i] + "]:");

               //AssignRoles(players);
            }
        }

        static void AssignRoles(string[] players)
        {
            int numberofSpies = GetNumSpies(players.Length);

            int role = random.Next(0, 1);
            if (role < 1)
            {
                Console.WriteLine("Resistance");
            }
            else
            {
                Console.WriteLine("Spy");
            }

            //string[] playerRole;
            //playerRole = new string[2];
            //playerRole[0] = ": Resistance";
            //playerRole[1] = ": Spy";
            //Random r = new Random();
            //int num = r.Next(playerRole.Length);

            //int role = random.Next(0, 1);
            //int SpyLimit = 0;

            //if (role < 1 )
            //{
            //    SpyLimit++;
            //    Console.WriteLine("Spy");
            //    if (SpyLimit > GetNumSpies(SpyLimit))
            //    {
            //        Console.WriteLine("Resistance");
            //    }
            //}
            //else
            //{
            //    Console.WriteLine("Resistance");
            //}

            //string[] printplayers = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
            //for (int i = 0; i < players.Length; i++)
            //{
            //    Console.WriteLine("Player [" + printplayers[i] + players[i] +"]");
            //}
            //Console.ReadKey();

            //int playerRole = random.Next(0, 2);
            //int spylimit = 0;
            //if (playerRole < 1)
            //{
            //    spylimit++;
            //    Console.WriteLine("Spy");
            //    if (spylimit > GetNumSpies(spylimit))
            //    {
            //        Console.WriteLine("Resistance");
            //    }
            //}
            //else
            //{
            //    Console.WriteLine("Resistance");
            //}

            // Must be dependent on the number of spies
            // Must always reach max # of spies. e.g. if there are 5 players there are always 2 spies
            // Must not exceed the # of spies for that set 
            // e.g if there are 7 players, there are always 3 spies, nothing less nothing more
            PrintPlayers(players);        
        }

        static int GetPlayersForMission(int totalPlayers)
        {
            int PlayersinMission = 0;
            int numberofplayers = 0;
            switch (numberofplayers)
            {
                case 5:
                case 6:
                    PlayersinMission = 3;
                    Console.WriteLine(PlayersinMission);
                    break;
                case 7:
                case 8:
                    PlayersinMission = 4;
                    Console.WriteLine(PlayersinMission);
                    break;
                case 9:
                case 10:
                    PlayersinMission = 5;
                    Console.WriteLine(PlayersinMission);
                    break;
            }
            // 5-6 players = 3 per mission
            // 7-8 players = 4 per mission
            // 9-10 players = 5 per mission
            return 0;
        }

        static bool ValidateIndex(int[] missionPlayersIndex, int randomIndex)
        {
            for (int j = 0; j < missionPlayersIndex.Length; j++)
            {
                int presentIndex = missionPlayersIndex[j];
                if (presentIndex == randomIndex)
                {
                    return true;
                }
            }
            return false;
        }

        static void Main(string[] args)
        {

            Console.WriteLine("Welcome to the Resistance!");

            int playersNum;

            while (true)
            {
                playersNum = 0;
                Console.Write("Enter number of players (5-10)");
                playersNum = Convert.ToInt32(Console.ReadLine());
                if (playersNum >= 5 && playersNum <= 10)
                    break;
                Console.Write("Invalid Input");
                Console.ReadKey();
                Console.Clear();
            }

            Console.Clear();
            Console.Write("Number of players: " + playersNum);
            Console.ReadKey();
            // Make an array of players based on the number the player entered
            string[] players = new string[playersNum];
            // Assign roles to the players
            AssignRoles(players);
            Console.Read();
            int resistanceScore = 0;
            int spiesScore = 0;
            int roundNum = 1;
            // Begin the missions
            while (resistanceScore < 3 && spiesScore < 3)
            {
                Console.Clear();
                Console.WriteLine("Mission " + roundNum.ToString());
                Console.WriteLine("Resistance: " + resistanceScore.ToString());
                Console.WriteLine("Spies: " + spiesScore.ToString());
                // Select random players based on the players array
                int missionPlayerCount = GetPlayersForMission(playersNum);
                List<int> missionPlayersIndex = new List<int>();
                string[] missionPlayers = new string[missionPlayerCount];
                for (int i = 0; i < missionPlayerCount; i++)
                {
                    int randomIndex = 0;
                    bool alreadyTaken = true;
                    while (alreadyTaken)
                    {
                        randomIndex = random.Next(0, players.Length);
                        alreadyTaken = ValidateIndex(missionPlayersIndex.ToArray(), randomIndex);
                    }
                    missionPlayersIndex.Add(randomIndex);
                    missionPlayers[i] = players[randomIndex];
                }
                // END select random players
                // Play the mission
                int result = EvaluateRound(missionPlayers);
                // Add the result score
                if (result == 0)
                {
                    resistanceScore++;
                }
                else
                {
                    spiesScore++;
                }
                roundNum++;
            }
            // END missions
            if (resistanceScore >= 3)
            {
                Console.WriteLine("Resistance Wins!");
            }
            else if (spiesScore >= 3)
            {
                Console.WriteLine("Spies Wins!");
            }
            Console.ReadKey();
        }
    }
}





