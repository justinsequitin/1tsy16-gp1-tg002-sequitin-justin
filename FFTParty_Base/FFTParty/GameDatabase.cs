﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFTParty
{
    public static class GameDatabase
    {
        public static Character GetCharacter(int id)
        {
            switch (id)
            {
                case 1: return new Character(1, "Knight", 50, 100);
                case 2: return new Character(2, "Squire", 10, 20);
                case 3: return new Character(3, "Chemist", 50, 100);
                case 4: return new Character(4, "Monk", 70, 200);
                case 5: return new Character(5, "Archer", 50, 100);
                case 6: return new Character(6, "Wizard", 100, 500);
                case 7: return new Character(7, "Summoner", 120, 600);
                case 8: return new Character(8, "Thief", 10, 80);
                case 9: return new Character(9, "Oracle", 130, 600);
                case 10: return new Character(10, "Dragoon", 200, 1200);
                default: return null;
            }
        }

        public static Monster GetMonster(int id)
        {
            switch (id)
            {
                case 1: return new Monster("Bone Soul", "Knight");
                case 2: return new Monster("Chocobo", "Squire");
                case 3: return new Monster("Goblin", "Chemist");
                case 4: return new Monster("Aevis", "Monk");
                case 5: return new Monster("Treant", "Archer");
                case 6: return new Monster("Mindflayer", "Wizard");
                case 7: return new Monster("Ghost", "Summoner");
                case 8: return new Monster("Minatour", "Thief");
                case 9: return new Monster("Hydra", "Oracle");
                case 10: return new Monster("Dragon", "Dragoon");
                default: return null;
            }
        }

        public static City GetCity(int id)
        {
            City city = null;
            if (id == 1)
            {
                city = new City("Merchant City of Dorter");
                city.RecruitableCharacters.Add(GetCharacter(2));
                city.RecruitableCharacters.Add(GetCharacter(3));
            }
            else if (id == 2)
            {
                city = new City("Magick City of Gariland");
                city.RecruitableCharacters.Add(GetCharacter(6));
                city.RecruitableCharacters.Add(GetCharacter(7));
                city.RecruitableCharacters.Add(GetCharacter(9));
            }
            else if (id == 3)
            {
                city = new City("Royal City of Lesalia");
                city.RecruitableCharacters.Add(GetCharacter(1));
                city.RecruitableCharacters.Add(GetCharacter(10));
            }
            else if (id == 4)
            {
                city = new City("Port City of Warjilis");
                city.RecruitableCharacters.Add(GetCharacter(8));
                city.RecruitableCharacters.Add(GetCharacter(5));
            }
            else if (id == 5)
            {
                city = new City("Clockwork City of Goug");
                city.RecruitableCharacters.Add(GetCharacter(3));
                city.RecruitableCharacters.Add(GetCharacter(9));

            }
            else if (id == 6)
            {
                city = new City("Free City of Bervenia");
                city.RecruitableCharacters.Add(GetCharacter(1));
                city.RecruitableCharacters.Add(GetCharacter(2));
                city.RecruitableCharacters.Add(GetCharacter(4));
            }

            return city;
        }
    }
}
