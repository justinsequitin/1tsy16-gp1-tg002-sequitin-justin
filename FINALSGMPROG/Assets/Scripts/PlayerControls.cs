﻿using UnityEngine;
using System.Collections;

public class PlayerControls : MonoBehaviour {
	public GameObject PlayerBullet;
	public GameObject BulletPosition;
	private Vector2 MousePosition;
	public float MoveSpeed = 50.0f;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown ("space")) {
			GameObject Bullet = (GameObject)Instantiate (PlayerBullet);
			Bullet.transform.position = BulletPosition.transform.position;
		}
		
		MousePosition = Input.mousePosition;
		MousePosition = Camera.main.ScreenToWorldPoint(MousePosition);
		transform.position = Vector2.Lerp(transform.position, MousePosition, MoveSpeed * Time.deltaTime);
		
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if ((col.tag == "EnemyShipTag") || (col.tag == "EnemyBulletTag")) {
			Destroy(gameObject);
		}
	}

}
