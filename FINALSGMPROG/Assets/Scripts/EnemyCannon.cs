﻿using UnityEngine;
using System.Collections;

public class EnemyCannon : MonoBehaviour {
	public GameObject BulletEnemy;
	// Use this for initialization
	void Start () {
	
		Invoke ("FireEnemyBullet", 1f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FireEnemyBullet()
	{
		GameObject playerShip = GameObject.Find("Player");

		if (playerShip != null) {
			GameObject bullet = (GameObject)Instantiate(BulletEnemy);
			bullet.transform.position = transform.position;

			Vector2 direction = playerShip.transform.position- bullet.transform.position;
			bullet.GetComponent<EnemyBullet>().SetDirection(direction);
		}
	}
}
