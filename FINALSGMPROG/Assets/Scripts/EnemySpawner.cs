﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {
	public GameObject Enemy;

	float maxSpawnRateinSeconds = 3f;

	// Use this for initialization
	void Start () {
	
		Invoke ("SpawnEnemy", maxSpawnRateinSeconds);

		InvokeRepeating ("IncreaseSpawn", 0f, 15f);


	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void SpawnEnemy()
	{
		Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));

		Vector2 max = Camera.main.ViewportToWorldPoint (new Vector2 (1, 1));

		GameObject anEnemy = (GameObject)Instantiate (Enemy);

		anEnemy.transform.position = new Vector2 (Random.Range (min.x, max.x), max.y);

		NextEnemySpawn ();
	}

	void NextEnemySpawn()
	{
		float spawninseconds;


		if (maxSpawnRateinSeconds > 1f) {
			spawninseconds = Random.Range (1f, maxSpawnRateinSeconds);
		} else
			spawninseconds = 1f;

		Invoke ("SpawnEnemy", spawninseconds);
	}

	void IncreaseSpawn()
	{
		if (maxSpawnRateinSeconds > 1f)
			maxSpawnRateinSeconds --;
		if(maxSpawnRateinSeconds == 1f)
			CancelInvoke ("IncreaseSpawn");

	}
}



















