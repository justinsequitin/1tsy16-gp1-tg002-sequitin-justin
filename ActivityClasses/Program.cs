﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActClasses
{
    class Program
    {
        
        public class Player
        {
            public int hp = 100;
            public int dmg = 15;
        }
        public class Enemy
        {
            public int hp = 100;
            public int dmg = 10;
        }
        public void GetClass()
        {
            int _class = 0;
            _class = Convert.ToInt32(Console.ReadLine());

            switch (_class)
            { 
                case 1: // Warrior
                    Console.WriteLine("Warrior");
                    break;
                case 2: // Mage
                    Console.WriteLine("Mage");
                    break;
                case 3: // Bruiser
                    Console.WriteLine("Bruiser");
                    break;
            }

        }

        static void Main(string[] args)
        {
      
            Player player = new Player();
            Console.WriteLine("Enter you desired name: ");
            Console.ReadLine();
                          
            Console.WriteLine("Choose your class: ");
            Console.WriteLine("1) Warrior 2) Mage 3) Bruiser");
            Console.ReadLine();   

            Console.WriteLine("Player Hp: " + player.hp);
            Console.WriteLine("Player Damage: " + player.dmg);

            Console.ReadKey();



        }
    }
}





