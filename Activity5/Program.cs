﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Activity5
{
    class Program
    {
        class Monster
        {

            public string name;
            public int level;
            public int hp;

            public void GetName()
            {
                Console.WriteLine("Input Name:");
                name = Console.ReadLine();
            }

            public void GetLevel()
            {
                Console.WriteLine("Input Level:");
                level = Convert.ToInt32(Console.ReadLine());
            }

            public void GetHp()
            {
                Console.WriteLine("Input Hp:");
                hp = Convert.ToInt32(Console.ReadLine());
            }

            public void ToPrint()
            {
                Console.WriteLine("Name: " + name);
                Console.WriteLine("Level: " + level);
                Console.WriteLine("Hp: " + hp);
            }


            static void Main(string[] args)
            {
                Monster monster = new Monster();
            }
        }
    }
}
