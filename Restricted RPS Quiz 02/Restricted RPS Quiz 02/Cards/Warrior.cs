﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Warrior : Card
    {
        public Warrior()
        {
            base.Name = "Warrior";
        }

        public override FightResult Fight(Card opponentCard)
        {

            if (opponentCard.Name == "Assassin")
            {
                Console.WriteLine("You have defeated the {0} of the enemy, you WON", opponentCard.Name);
                return FightResult.Win;
            }
            else if (opponentCard.Name == "Warrior")
            {
                Console.WriteLine("the battle between {0} of the enemy and you resulted to DRAW", opponentCard.Name);
                return FightResult.Draw;
            }
            else if (opponentCard.Name == "Mage")
            {
                Console.WriteLine("The Enemy's {0} has DEFEATED you!", opponentCard.Name);
                return FightResult.Lose;
            }
            return 0;
        }
    }
}
