﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public abstract class Commander
    {
        public string Name { get; set; }
        public int Points { get; protected set; }

        public List<Card> cards = new List<Card>();
        public IList<Card> Cards { get { return cards; } }
        public bool EmptyHand { get { return cards.Count == 0; } }

        /// <summary>
        /// Can only discard if commander has at least 2 cards.
        /// </summary>
        public bool CanDiscard
        {
          
            get
            {
                // List if the Commander has more than 2 Cards (if - else statement)
                if (cards.Count > 2)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Draw a random card and add it to your hand.
        /// </summary>
        /// <returns></returns>
        public Card Draw()
        {
            int summoningHero = RandomHelper.Range(3);
            switch (summoningHero)
            {
                case 0: cards.Insert(0, new Warrior());
                    if (Name == "Player") { Console.WriteLine("You have summoned a Warrior!"); }
                    break;
                case 1: cards.Insert(0, new Mage());
                    if (Name == "Player") { Console.WriteLine("You have summoned a Mage!"); }
                    break;
                case 2: cards.Insert(0, new Assassin());
                    if (Name == "Player") { Console.WriteLine("You have summoned an Assassin!"); }
                    break;
            }
            return null;
        }

        /// <summary>
        /// Discards two random cards in exchange of one random card. Cannot discard if player has only one card (throws an exception)
        /// </summary>
        /// <returns>Received card after discarding.</returns>
        public Card Discard()
        {
            if (!CanDiscard) throw new Exception("Cannot discard now."); // DON'T DELETE THIS LINE
            else if (CanDiscard)
            {
                Console.WriteLine("{0} sacrificed 2 units to summon a new Hero!", Name);
                for (int p = 0; p < 2; p++)
                {
                    int discardingCardIndex = RandomHelper.Range(cards.Count - 1);
                    //Console.WriteLine("Removing {0} Number : {1}" ,cards[discardingCardIndex].Name, discardingCardIndex);
                    cards.RemoveAt(discardingCardIndex);
                }
                Draw();
            }
             
               Console.ReadKey();
            
            return null;
        }

        /// <summary>
        /// Display this commander's cards. NOTE: Only call this for the player's turn.
        /// </summary>
        public void DisplayCards()
        {
            if (Name == "Player")
            {
                Console.WriteLine("Cards on Hand");
                for (int x = 0; x < cards.Count; x++)
                {
                    Console.WriteLine(cards[x].Name);
                }
            }
        }

        /// <summary>
        /// Called whenever one side's hand is empty. All cards will be discarded. Each discarded card will reduce the player's point by 1.
        /// </summary>
        public void OnGameEnding()
        {
            if (cards.Count != null)
            {
                Points -= cards.Count;
            }
            else
            {
            }
        }

        /// <summary>
        /// Commander's action upon his turn. Can either "Play" or "Discard"
        /// </summary>
        /// <param name="opponent"></param>
        public abstract void EvaluateTurn(Commander opponent);

        /// <summary>
        /// Choose a card to play. Card must be discarded after playing.
        /// </summary>
        /// <returns>Card to play.</returns>
        public abstract Card PlayCard();
        /// <summary>
        /// Each commander plays a card. Points are evaluated here.
        /// Free code :)
        /// </summary>
        /// <param name="opponent"></param>
        public void Fight(Commander opponent)
        {
            Card myCard = PlayCard();
            Card opponentCard = opponent.PlayCard();

            Console.WriteLine("{0} has sent {1} to Battle!", Name, myCard.Name);
            Console.WriteLine("{0} has sent {1} to Battle!", opponent.Name, opponentCard.Name);
            FightResult result = myCard.Fight(opponentCard);
            if (result == FightResult.Win)
            {
                Points += 2;
            }
            
        }
    }
}
