﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class AI : Commander
    {
        /// <summary>
        /// 30% chance to discard, 70% chance to play a card.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            Random chance = new Random();
            int choose = chance.Next(0, 10);
            if (choose > 3 && cards.Count >= 2)
            {
                Discard();
            }
            else
            {
                Console.WriteLine("{0} has challenged {1} to a DUEL!", Name, opponent.Name);
                Console.WriteLine();
                opponent.DisplayCards();
                Fight(opponent);
            }
        }

        public override Card PlayCard()
        {
            DisplayCards();
            Card played;
            int chooseCardIndex = RandomHelper.Range(cards.Count);
            played = cards[chooseCardIndex];
            cards.RemoveAt(chooseCardIndex);
            return played;
        }
    }
}
