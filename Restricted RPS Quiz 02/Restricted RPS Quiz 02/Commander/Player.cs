﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Player : Commander
    {
        /// <summary>
        /// Get player's input. Player can only either Play or Discard.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            DisplayCards();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("[1] Challenge AI");
            Console.WriteLine("[2] Discard a Card");
            int decision = Convert.ToInt32(Console.ReadLine());
            switch (decision)
            {
                case 1:
                    Console.WriteLine("{0} Challanged AI to fight!", Name, opponent.Name);
                       Fight(opponent);
                    break;
                case 2:
                    if (CanDiscard)
                    {
                       Discard();
                    }
                    break;
            } 
        }

        /// <summary>
        /// Show a list of cards to choose from. If there are 2 cards of the same type (eg. Warrior), only show one of each type.
        /// </summary>
        /// <returns></returns>
        public override Card PlayCard()
        {
            Console.WriteLine("Selec Card to Play...");
            List<string> CardsToChoose = cards.Select(itemtoGet => itemtoGet.Name).Distinct().ToList();  
            while (true)
            {
            for (int x = 0; x < CardsToChoose.Count; x++)
            {
                int y = x + 1;
                Console.WriteLine("[{0}] {1}", y, CardsToChoose[x]);
            }
                int intoBattle = Convert.ToInt32(Console.ReadLine()) - 1;
                Card played;
                if (cards.Exists(p => p.Name == CardsToChoose[intoBattle])) // will check if the name of the chosen hero exists in his HANDS
                {
                    int indextoSend = cards.FindIndex(p => p.Name == CardsToChoose[intoBattle]);
                    played = cards[indextoSend];
                    cards.RemoveAt(indextoSend);
                    return played;
                }
                else
                {
                    Console.WriteLine("Invalid Input!");

                }
            }
            
           
        }
    }
}
