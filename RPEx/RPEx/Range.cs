﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPEx
{
    public struct Range
    { 
        public int Low;
        public int High;
    }
}