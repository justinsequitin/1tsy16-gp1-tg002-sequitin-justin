﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPEx
{
    public class Player
    {
        public string Name { get; private set; }
        public int classNum { get; private set; }
        // Constructor
        public Player()
        {
            Name = "Default";
            raceName = "Default";
            className = "Default";
            classNum = 0;
            manaPoints = 0;
            maxManaPoints = 0;
            Inventory = Storage.Count;
            accuracy = 0;
            hitPoints = 0;
            gold = 0;
            maxHitPoints = 0;
            expPoints = 0;
            nextLevelExp = 0;
            level = 0;
            Armor = 0;
            Luck.Low = level * accuracy;
            Luck.High = level * accuracy * 2;
            weapon = new Weapon("Default Weapon Name", 0, 0, 0);
        }

        public void CreateClass()
        {

            Console.WriteLine("CHARACTER CLASS GENERATION");
            Console.WriteLine("==========================");

            // Input character's name.
            Console.WriteLine("Enter your character's name: ");
            Name = Console.ReadLine();

            Console.WriteLine("Please select a Race number...");
            Console.WriteLine("1.)Human 2.)Elf 3.)Undead 4.)Bellato");

            int characterRace = 0;

            characterRace = Convert.ToInt32(Console.ReadLine());
            
            switch (characterRace)
            { 
                case 1: // Human
                    raceName = "Human";
                    gold += 100;
                    accuracy -= 2;
                    hitPoints += 15;
                    maxHitPoints += 15;
                    manaPoints += 10;
                    maxManaPoints += 10;
                    Armor += 2;
                    break;
                case 2: //Elf
                    raceName = "Elf";
                    gold += 10;
                    accuracy += 5;
                    hitPoints -= 5;
                    maxHitPoints -= 5;
                    manaPoints += 10;
                    maxManaPoints += 10;
                    Armor += 3;
                    break;
                case 3: //Undead
                    raceName = "Undead";
                    gold += 25;
                    accuracy += 2;
                    hitPoints += 5;
                    maxHitPoints += 5;
                    manaPoints += 10;
                    maxManaPoints += 10;
                    Armor -= 2;
                    break;
                case 4: //Bellato
                    raceName = "Bellato";
                    gold += 150;
                    accuracy += 4;
                    hitPoints -= 15;
                    maxHitPoints -= 15;
                    manaPoints += 10;
                    maxManaPoints += 10;
                    Armor += 1;
                    break;
            }

            Console.WriteLine("Please select a character class number...");
            Console.WriteLine("1)Fighter 2)Wizard 3)Cleric 4)Thief : ");


            int classNum = Convert.ToInt32(Console.ReadLine());

            switch (classNum)
            {
                case 1: //Fighter
                    className = "Fighter";
                    accuracy += 10;
                    hitPoints += 20;
                    maxHitPoints += 20;
                    manaPoints += 20;
                    maxManaPoints += 20;
                    newSpell.Add(new Spell("Crushing Blow!", 5, 10, 10));
                    newSpell.Add(new Spell("Whirlwind Cleave!", 5, 8, 5));
                    expPoints += 0;
                    nextLevelExp = 1000;
                    level += 1;
                    Armor += 4;
                    weapon = new Weapon("Long Sword", 1, 8, 0);

                    break;
                case 2: //Wizard
                    className = "Wizard";
                    accuracy = 5;
                    hitPoints = 10;
                    maxHitPoints = 10;
                    manaPoints += 40;
                    maxManaPoints += 40;
                    newSpell.Add(new Spell("Firewall!", 15, 20, 15));
                    newSpell.Add(new Spell("Petrify!", 20, 25, 20));
                    expPoints = 0;
                    nextLevelExp = 1000;
                    level += 1;
                    Armor += 1;
                    weapon = new Weapon("Staff", 1, 4, 0);
                    break;
                case 3:
                    className = "Cleric";
                    accuracy = 8;
                    hitPoints = 15;
                    maxHitPoints = 15;
                    manaPoints += 30;
                    maxManaPoints += 30;
                    newSpell.Add(new Spell("Armor Break!", 15, 20, 10));
                    newSpell.Add(new Spell("Healing Ward!", 15, 20, 10));
                    expPoints = 0;
                    nextLevelExp = 1000;
                    level += 1;
                    Armor += 3;
                    weapon = new Weapon("Flail", 1, 6, 0);
                    break;
                default: //
                    className = "Thief";
                    accuracy = 7;
                    hitPoints = 12;
                    maxHitPoints = 12;
                    manaPoints += 20;
                    maxManaPoints += 20;
                    newSpell.Add(new Spell("Blinding Smoke!", 15, 20, 10));
                    newSpell.Add(new Spell("Double Strike!", 5, 10, 5));
                    expPoints = 0;
                    nextLevelExp = 1000;
                    level += 1;
                    Armor += 2;
                    weapon = new Weapon("Dagger", 1, 6, 0);
                    break;
            }
        }

        public bool isDead { get { return hitPoints <= 0; } }
        public int Armor { get; private set; }
        public bool Attack(Monster monsterTarget)
        {
            int selection = 1;
            Console.Write("1) Attack, 2) Cast Spell, 3) Use Item ,4) Run");
            selection = Convert.ToInt32(Console.ReadLine());
            switch (selection)
            {
                case 1:

                    Console.WriteLine("You attack an " + monsterTarget.Name + " with a " + weapon.Name);
                    if (RandomHelper.Random(0, 20) < accuracy)
                    {
                        int damage = RandomHelper.Random(weapon.DamageRange);
                        int totalDamage = damage - monsterTarget.Armor;
                        if (totalDamage <= 0)
                        {
                            Console.WriteLine("Your attack failed to penetrate the armor");
                        }
                        else
                        {
                            Console.WriteLine("You attack for " + totalDamage.ToString() + " damage!");
                            monsterTarget.TakeDamage(totalDamage);
                        }
                    }
                    else
                    {
                        Console.WriteLine("You miss!");
                    }
                    break;
                case 2:
                    Console.WriteLine(" 1.)" + newSpell[0].Name + "  2.)" + newSpell[1].Name);
                    int castNum;
                    castNum = Convert.ToInt32(Console.ReadLine());
                    switch (castNum)
                    {
                        case 1:
                            if (manaPoints >= newSpell[0].MagicPointsRequired)
                            {
                                Console.WriteLine("You have used " + newSpell[0].Name + "!!");
                                if (RandomHelper.Random(0, 10) < accuracy)
                                {
                                    int damage = RandomHelper.Random(newSpell[0].DamageRange);
                                    int totalDamage = 0;
                                    totalDamage = damage - monsterTarget.Armor;
                                    if (totalDamage <= 0)
                                    {
                                        Console.WriteLine("Your attack failed to penetrate the armor");
                                        manaPoints -= newSpell[0].MagicPointsRequired;
                                    }
                                    else
                                    {
                                        Console.WriteLine("You attack for " + totalDamage.ToString() + " damage!");
                                        monsterTarget.TakeDamage(totalDamage);
                                        manaPoints -= newSpell[0].MagicPointsRequired;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("But you have missed!!");
                                }
                                break;
                            }
                            else
                            {
                                Console.WriteLine("Insufficient Mana! cant use this Magic!");
                                monsterTarget.TakeDamage(0);
                                break;
                            }
                        case 2:
                            if (manaPoints >= newSpell[0].MagicPointsRequired)
                            {
                                Console.WriteLine("You have used " + newSpell[1].Name + "!!");
                                if (RandomHelper.Random(0, 20) < accuracy)
                                {
                                    int damage = RandomHelper.Random(newSpell[1].DamageRange);
                                    int totalDamage = 0;
                                    if (newSpell[1].Name == "Armor Break!")
                                    {
                                        Console.WriteLine(newSpell[1].Name + " has Broken the enemy Armor! Damage Increase!");
                                        totalDamage = damage - (monsterTarget.Armor - RandomHelper.Random(newSpell[1].DamageRange));
                                        manaPoints -= newSpell[1].MagicPointsRequired;
                                    }
                                    else if (newSpell[1].Name == "Healing Ward!")
                                    {
                                        int Heal = RandomHelper.Random(newSpell[1].DamageRange);
                                        Console.WriteLine("You have heal yourself for " + Heal + " Health!");
                                        hitPoints += Heal * level;
                                        manaPoints -= newSpell[1].MagicPointsRequired;

                                    }
                                    else if (newSpell[1].Name == "Double Strike!")
                                    {
                                        Console.WriteLine("The Attack was Double!!!");
                                        totalDamage = damage * 2 - (monsterTarget.Armor - RandomHelper.Random(newSpell[1].DamageRange));
                                    }
                                    else
                                    {
                                        totalDamage = damage - monsterTarget.Armor;
                                        manaPoints -= newSpell[1].MagicPointsRequired;
                                    }
                                    if (totalDamage <= 0)
                                    {
                                        Console.WriteLine("Your attack failed to penetrate the armor");
                                        manaPoints -= newSpell[1].MagicPointsRequired;
                                    }
                                    else
                                    {
                                        Console.WriteLine("You attack for " + totalDamage.ToString() + " damage!");
                                        monsterTarget.TakeDamage(totalDamage);
                                    }
                                    break;

                                }
                                break;
                            }
                            else
                            {
                                Console.WriteLine("Insufficient Mana! cant use this Magic!");
                                monsterTarget.TakeDamage(0);
                                break;
                            }
                    }
                    break;
                case 3:
                    Console.WriteLine("Which Potion do you want to use?");
                    for (int x = 0; x < potion.Count; x++)
                    {
                        Console.WriteLine("Potion No. " + x);
                    }
                    int drink = Convert.ToInt32(Console.ReadLine()) - 1;
                    if (drink <= potion.Count)
                    {
                        Console.WriteLine(potion[drink].brand + " has been used!");
                        hitPoints += potion[drink].health;
                        potion.RemoveAt(drink - 1);
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Potion Not Found.");
                    }
                    break;
                case 4:
                    // 25% chance of being able to run
                    int roll = RandomHelper.Random(1, 4);
                    if (roll == 1)
                    {
                        Console.WriteLine("You run away!");
                        return true; //<-- Return out of the function
                    }
                    else
                    {
                        Console.WriteLine("You could not escape!");
                    }
                    break;
            }

            return false;
        }

        public void TakeDamage(int damage)
        {
            hitPoints -= damage;
            if (hitPoints < 0)
            {
                hitPoints = 0;
            }
        }

        public void LevelUp(int number)
        {
            if (expPoints >= nextLevelExp)
            {
                Console.WriteLine("You gained a level!");

                // Increment level.
                level++;

                // Set experience points requard for next level
                nextLevelExp = level * level * 1000;
                // Increase stats depending on Character Class
                switch (number)
                {
                    case 1:
                        accuracy += RandomHelper.Random(1, 4);
                        maxHitPoints += RandomHelper.Random(5, 10);
                        Armor += RandomHelper.Random(4, 5);

                        break;
                    case 2:
                        accuracy += RandomHelper.Random(3, 4);
                        maxHitPoints += RandomHelper.Random(4, 8);
                        Armor += RandomHelper.Random(2, 4);
                        break;
                    case 3:
                        accuracy += RandomHelper.Random(2, 3);
                        maxHitPoints += RandomHelper.Random(7, 9);
                        Armor += RandomHelper.Random(3, 5);
                        break;
                    case 4:
                        accuracy += RandomHelper.Random(6, 8);
                        maxHitPoints += RandomHelper.Random(5, 6);
                        Armor += RandomHelper.Random(3, 5);
                        break;
                }
                // Give the player full hitpoints when they level up.
                hitPoints = maxHitPoints;
            }
        }

        public void Rest()
        {
            Console.WriteLine("Resting...");

            hitPoints = maxHitPoints;
            manaPoints = maxManaPoints;

            // TODO: Modify the function so that random enemy enounters
            // are possible when resting


        }

        public void ViewStats()
        {
            Console.WriteLine("PLAYER STATS");
            Console.WriteLine("============");
            Console.WriteLine();

            Console.WriteLine("Name             = " + Name);
            Console.WriteLine("Race             = " + raceName);
            Console.WriteLine("Class            = " + className);
            Console.WriteLine("Accuracy         = " + accuracy.ToString());
            Console.WriteLine("Hitpoints        = " + hitPoints.ToString());
            Console.WriteLine("MaxHitpoints     = " + maxHitPoints.ToString());
            Console.WriteLine("Gold             = " + gold.ToString());
            Console.WriteLine("XP               = " + expPoints.ToString());
            Console.WriteLine("XP to next level = " + nextLevelExp.ToString());
            Console.WriteLine("Level            = " + level.ToString());
            Console.WriteLine("Armor            = " + Armor.ToString());
            Console.WriteLine("Weapon Name      = " + weapon.Name);
            Console.WriteLine("Weapon Damage    = " + weapon.DamageRange.Low.ToString() + "-" + weapon.DamageRange.High.ToString());

            Console.WriteLine();
            Console.WriteLine("END PLAYER STATS");
            Console.WriteLine("================");
            Console.WriteLine();
        }

        public void Victory(int xp, int loot)
        {
            Console.WriteLine("You won the battle!");
            Console.WriteLine("You win " + xp.ToString() + " experience points!");
            Console.WriteLine("You loot " + loot.ToString() + " gold!");
            int weaponLootchance = 5;
            if(weaponLootchance == 5)
            {
                Range itemQuality;
                itemQuality.Low = 5;
                itemQuality.High = 25;
                int itemShouter = Storage.Count;
                if (Inventory >= 10)
                {
                    Console.WriteLine("Inventory is Full! Would you like to drop an Item?");
                    Console.WriteLine("1) Y  2) N ");
                    int drop = Convert.ToInt32(Console.ReadLine());
                    if(drop == 1)
                    {

                        for(int x = 0; x < Storage.Count; x++)
                        {
                            Console.WriteLine("Item No."+x +"   " +Storage[x].Name);
                        }
                        Console.Write("choose an Item Number to Drop : ");
                        int dropIT = Convert.ToInt32(Console.ReadLine());
                        Storage.RemoveAt(dropIT-1);
                        Console.WriteLine("Previous Item No." + dropIT + "has been dropped to the world.");
                     }
                }
                else if(className == "Fighter" && Inventory <= 10)
                {
                    Storage.Add(new Weapon("Fist of Fury", RandomHelper.Random(itemQuality), RandomHelper.Random(itemQuality), 0));
                    Console.WriteLine("You have Obtained " + Storage[itemShouter].Name);
                }
                else if (className == "Wizard" && Inventory <= 10)
                {
                    Storage.Add(new Weapon("Staff of Lightning", RandomHelper.Random(itemQuality), RandomHelper.Random(itemQuality), 0));
                    Console.WriteLine("You have Obtained " + Storage[itemShouter].Name);
                }
                else if (className == "Cleric" && Inventory <= 10)
                {
                   Storage.Add(new Weapon("Flail of Light", RandomHelper.Random(itemQuality), RandomHelper.Random(itemQuality), 0));
                   Console.WriteLine("You have Obtained " + Storage[itemShouter].Name);
                }
                else if (className == "Thief" && Inventory <= 10)
                {
                    Storage.Add(new Weapon("Dagger of Time", RandomHelper.Random(itemQuality), RandomHelper.Random(itemQuality), 0));
                    Console.WriteLine("You have Obtained " + Storage[itemShouter].Name);
                }

                else
                {
                    Console.WriteLine("Item Dropped by Enemy is gone!");
                    Console.ReadKey();
                }
                
               
             }
            
            expPoints += xp;
            gold += loot;
        }


        public void Store()
        {
            storeArmor.Add(new Armor("Platemail", 5, 10, 10));
            storeArmor.Add(new Armor("Heavy Armor", 10, 15, 20));
            storeArmor.Add(new Armor("Legendary Armor", 25, 35, 25));

            storeWeapon.Add(new Weapon("BusterSword", 5, 10, 5));
            storeWeapon.Add(new Weapon("MightFang", 15, 20, 15));
            storeWeapon.Add(new Weapon("BrineSword", 25, 30, 20));

            storePotion.Add(new Potion("Health Potion", 25, 10));
            storePotion.Add(new Potion("Mana Potion", 25, 10));
            storePotion.Add(new Potion("Bothe Potion", 25, 20));


            bool playerIN = true;
            while (playerIN)
            {
                Console.WriteLine("Hello " + Name + " what do you wish to buy ?");
                Console.WriteLine("1) Armor 2)Weapon 3) Potions 4) Sell 5) Leave");
                int decision = Convert.ToInt32(Console.ReadLine());
                switch (decision)
                {
                    case 1:
                        Console.WriteLine("What Armor do you want to purchase?");
                        Console.WriteLine("1) " + storeArmor[0].Name + "2) " + storeArmor[1].Name + "3) " + storeArmor[2].Name);
                        int armorbuy = Convert.ToInt32(Console.ReadLine()) - 1;
                        int armorPrice = RandomHelper.Random(storeArmor[armorbuy].Price);
                        if (gold >= armorPrice)
                        {
                            int armorADD = RandomHelper.Random(storeArmor[armorbuy].ArmorRange);
                            Console.WriteLine("The [" + storeArmor[armorbuy].Name + "]  has been added to your Armor Rate!");
                            Armor += armorADD;
                            gold -= armorPrice;
                            Console.WriteLine("You only have " + gold + "  gold Left!");          // how would it bring back to the player?
                        }
                        else
                        {
                            Console.WriteLine("The Price is " + armorPrice + " for this one of a kind Item!");
                            Console.WriteLine("Insufficient Funds.");
                        }
                        break;
                    case 2:
                        Console.WriteLine("What Weapon do you want to purchase?");
                        Console.WriteLine("1) " + storeWeapon[0].Name + "2) " + storeWeapon[1].Name + "3) " + storeWeapon[2].Name);
                        int weaponbuy = Convert.ToInt32(Console.ReadLine()) - 1;
                        int weaponPrice = RandomHelper.Random(storeWeapon[weaponbuy].Price);
                        if (gold >= weaponbuy)
                        {
                            int armorADD = RandomHelper.Random(storeArmor[weaponbuy].ArmorRange);
                            Console.WriteLine("The [" + storeArmor[weaponbuy].Name + "]  has been automatically equipped!");
                            weapon.Name = storeWeapon[weaponbuy].Name;
                            weapon.DamageRange = storeWeapon[weaponbuy].DamageRange;
                            gold -= weaponbuy;
                            Console.WriteLine("You only have " + gold + "  gold Left!");          // how would it bring back to the player?
                        }
                        else
                        {
                            Console.WriteLine("The Price is " + weaponbuy + " for this one of a kind Item!");
                            Console.WriteLine("Insufficient Funds.");
                        }
                        break;
                    case 3:
                        Console.WriteLine("What Potion do you want to purchase?");
                        Console.WriteLine("1) " + storePotion[0].brand + "2) " + storePotion[1].brand + "3) " + storePotion[2].brand);
                        int potionbuy = Convert.ToInt32(Console.ReadLine()) - 1;
                        int potionPrice = RandomHelper.Random(storeWeapon[potionbuy].Price);
                        if (gold >= potionbuy)
                        {
                            int armorADD = RandomHelper.Random(storeArmor[potionbuy].ArmorRange);
                            Console.WriteLine("The [" + storeArmor[potionbuy].Name + "]  has been automatically sent to potion inventory!");
                            potion.Add(storePotion[potionbuy]);
                            gold -= potionbuy;
                            Console.WriteLine("You only have " + gold + "  gold Left!");          // how would it bring back to the player?
                        }
                        else
                        {
                            Console.WriteLine("The Price is " + potionbuy + " for this one of a kind Item!");
                            Console.WriteLine("Insufficient Funds.");
                        }
                        break;
                    case 4:
                        Console.WriteLine("What do you wish to sell?");
                        int weaponShouter = Storage.Count;

                        for (int x = -1; x < weaponShouter; x++)
                        {
                            if (x <= Storage.Count + 1)
                            {
                                Console.WriteLine("Item no. " + x + "   " + Storage[x].Name);

                                break;
                            }

                            else if( x > Storage.Count)
                            {
                                Console.WriteLine("You're Inventory is Empty.");
                                break;
                            }
                        }
                        int sellThis = Convert.ToInt32(Console.ReadLine());
                        if (sellThis <= Storage.Count)
                        {
                            gold += RandomHelper.Random(Storage[sellThis].Price);
                        }
                        else
                        {
                            Console.WriteLine("Item Not Found.");
                        }
                        break;
                    case 5:
                        playerIN = false;
                        break;

                }
            }
        }

        public void GameOver()
        {
            Console.WriteLine("You died in battle...");
            Console.WriteLine();
            Console.WriteLine("==========================");
            Console.WriteLine("GAME OVER!");
            Console.WriteLine("==========================");
            Console.ReadLine();
        }

        public void DisplayHitPoints()
        {
            Console.WriteLine(Name + "'s hitpoints = " + hitPoints.ToString());
            
        }
        
       
        private string className;
        public int hitPoints;
        private int maxHitPoints;
        private int expPoints;
        private int nextLevelExp;
        public int level;
        private int accuracy;
        private Weapon weapon;
        public int gold;
        private string raceName;
        private int manaPoints;
        private int maxManaPoints;
        public List<Spell> newSpell = new List<Spell>();
        public Range Luck;
        public List<Weapon> Storage = new List<Weapon>();
        public List<Potion> potion = new List<Potion>();
        public int Inventory;
        public List<Armor> storeArmor = new List<Armor>();
        public List<Weapon> storeWeapon = new List<Weapon>();
        public List<Potion> storePotion = new List<Potion>();
    }
}
    
    