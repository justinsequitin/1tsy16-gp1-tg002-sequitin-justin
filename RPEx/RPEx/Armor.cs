﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPEx
{
    class Armor
    {
         public Armor(string title, int lowArmorQuality, int highArmorQuality, int levelRequirement)
         { 
            Name = title;
            ArmorRange.Low = lowArmorQuality;
            ArmorRange.High = highArmorQuality;
            Price.Low = lowArmorQuality/2;
            Price.High = highArmorQuality / 2;
         }
         public string Name { get; set; }
         public Range ArmorRange;
         public Range Price;
    }
}
