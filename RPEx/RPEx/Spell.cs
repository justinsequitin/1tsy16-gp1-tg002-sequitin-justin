﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPEx
{
    public class Spell
    {
        // Copy Weapon Statistics
        public Spell(string name, int lower, int upper, int require)
        { 
            Name = name;
            DamageRange.Low = lower;
            DamageRange.High = upper;
            MagicPointsRequired = require;

        }

        public string Name;
        public Range DamageRange;
        public int MagicPointsRequired;
    }
}