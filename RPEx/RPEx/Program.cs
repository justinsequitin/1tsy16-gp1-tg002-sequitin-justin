﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPEx
{
    class Program
    {
        static void Main(string[] args)
        { 
            Map gameMap = new Map();
            Player mainPlayer = new Player();
            mainPlayer.CreateClass();

            // Begin adventure
            bool done = false;
            while (!done)
            {
                // Each loop cycle we output the player position
                // and a selection menu
                if (gameMap.PlayerXPos == 1 && gameMap.PlayerYPos == 1)
                {
                    mainPlayer.Store();
                }
                gameMap.PrintPlayerPos();
               
                int selection = 1;
                Console.Write("1) Move, 2) Rest, 3) View Stats 4)Inventory 5) Quit:");
                selection = Convert.ToInt32(Console.ReadLine());
                Monster monster = null;
                bool notINbattle = true;
                switch (selection)
                {
                    case 1:
                        if (notINbattle)
                        {
                            gameMap.MovePlayer();
                            // Check for a random encounter. This function
                            // returns a null pointer if no monster are
                            // encoutered.
                            notINbattle = false;
                        }
                        break;
                    case 2:
                        int encounter = RandomHelper.Random(0, 4);
                        if (encounter > 5)
                        {
                            mainPlayer.Rest();
                            break;
                        }
                        else
                        {
                            notINbattle = false;
                           
                        }
                        break;
                    case 3:
                        mainPlayer.ViewStats();
                        break;
                    case 4:
                        int weaponShouter = mainPlayer.Storage.Count;

                        for (int x = -1; x < weaponShouter; x++)
                        {
                            if (x <= mainPlayer.Storage.Count + 1)
                            {
                                Console.WriteLine("Item no. " + x + "   " + mainPlayer.Storage[x].Name);

                                break;
                            }

                            else if (x > mainPlayer.Storage.Count)
                            {
                                Console.WriteLine("You're Inventory is Empty.");
                                break;
                            }
                        }
                        break;
                    case 5:
                        done = true;
                        break;
                }
                if (!notINbattle)
                {

                    monster = gameMap.CheckRandomEncounter();
                    if (monster != null)
                    {
                        while (true)
                        {
                            // Display hitpoints
                            mainPlayer.DisplayHitPoints();
                            monster.DisplayHitPoints();
                            Console.WriteLine();

                            bool runAway = mainPlayer.Attack(monster);

                            if (runAway)
                                break;

                            if (monster.isDead)
                            {
                                mainPlayer.Victory(monster.ExpReward, monster.Loot);
                                mainPlayer.LevelUp(mainPlayer.classNum);
                                break;
                            }

                            monster.Attack(mainPlayer);

                            if (mainPlayer.isDead)
                            {
                                mainPlayer.GameOver();
                                done = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}
