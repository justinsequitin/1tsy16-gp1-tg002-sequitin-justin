﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPEx
{
    class Potion
    {
        public Potion(string name, int fixHealth, int fixPrice)
        { 
            brand = name;
            health = fixHealth;
            price = fixPrice;
        }
        public string brand { get; set; }
        public int health;
        public int price;
    }
}
