﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPEx
{
    public class Weapon
    {
        public Weapon(string name, int lowDamage, int highDamage, int levelRequirement)
        { 
            Name = name;
            DamageRange.Low = lowDamage;
            DamageRange.High = highDamage;
            Price.Low = lowDamage/2;
            Price.High = highDamage/2;
        }

        public string Name { get; set; }
        public Range DamageRange;
        public Range Price;
    }
}