﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Activity4
{
    public class Circle
    {
        private static double radius = 1.0;
        private static string color = "red";
        //static void Circle()
        //{
        // radius = 1.0;
        // color = "red";
        //}
        public void SetRadius(double newValue)
        {
            radius = newValue;
        }
        public void SetColor(string newColor)
        {
            color = newColor;
        }
        public double GetRadius()
        {
            return radius;
        }
        public double GetRadius(double multiplierValue)
        {
            return 0;
        }
        public string GetColor()
        {
            return color;
        }
    }
    //public class Archer
    //{
    // public string Name;
    // public int Hp;
    // public int Power;
    // private int AtkModifier();
    // public void Move();
    // public void BasicAttack();
    // public void Strafe();
    //}
    class Program
    {
        static void Main(string[] args)
        {
            //Activity
            Circle circle = new Circle();
            circle.GetRadius();
            circle.GetColor();
            Console.WriteLine("Radius:");
            Console.WriteLine(circle.GetRadius());
            Console.WriteLine("Color:");
            Console.WriteLine(circle.GetColor());
            //From user input
            Console.WriteLine("Enter New Radius:");
            double InputValue = Convert.ToDouble(Console.ReadLine());
            circle.SetRadius(InputValue);
            Console.WriteLine("Enter New Color:");
            string InputColor = Console.ReadLine().ToString();
            circle.SetColor(InputColor);
            Console.WriteLine(circle.GetRadius());
            Console.WriteLine(circle.GetColor());
            Console.ReadKey();
            //Multiplier
            Console.WriteLine(circle.GetRadius(5));
            Console.ReadKey();
            //Archer archer = new Archer();
        }
    }
}